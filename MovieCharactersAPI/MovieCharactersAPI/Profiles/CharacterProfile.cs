﻿using AutoMapper;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTOs;
using System.Linq;

namespace MovieCharactersAPI.Profiles
{
    public class CharacterProfile : Profile
    {
       public CharacterProfile()
        {
            CreateMap<Character,CharacterPostDTO>().ReverseMap();
            CreateMap<Character, CharacterReadDTO>().ReverseMap();
           CreateMap<Character, CharacterGetDTO>()
                .ForMember(cdto => cdto.MovieId, opt => opt
                .MapFrom(c => c.Movies.Select(m => m.MovieId).ToArray()));
            //.ReverseMap();

            CreateMap<Character, CharacterPutDTO>()
                .ForMember(cdto => cdto.CharacterId, opt => opt
                .MapFrom(c => c.CharacterId))
                .ReverseMap();
        }
        
    }
}
