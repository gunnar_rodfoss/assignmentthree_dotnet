﻿using AutoMapper;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTOs;
using System.Linq;

namespace MovieCharactersAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieGetAllDTO>()
                .ForMember(m => m.CharacterId, opt => opt
                .MapFrom(c => c.Characters.Select(c => c.CharacterId)
                .ToArray()));
            /**/CreateMap<Movie, MovieGetDTO>()
                .ForMember(m => m.FrenchiseId, opt => opt
                .MapFrom(f => f.FranchiseId))
                .ReverseMap();
            //.ReverseMap();
            //CreateMap<Movie, MoviePostDTO>().ReverseMap();
            /**/CreateMap<Movie, MoviePostDTO>()
             .ForMember(m => m.FrenchiseId, opt => opt
             .MapFrom(f => f.FranchiseId))
             .ReverseMap();

           // CreateMap<Movie, MovieGettDTO>().ReverseMap();
           // CreateMap<Movie, MoviePostDTO>().ReverseMap();
        }
    }
}
