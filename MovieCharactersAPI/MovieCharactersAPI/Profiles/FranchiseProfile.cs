﻿using AutoMapper;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTOs;
using System.Linq;

namespace MovieCharactersAPI.Profiles
{
    public class FranchiseProfile : Profile
    {

        

        public FranchiseProfile()
        {
            //CreateMap<Franchise, FranchiseDTO>().ReverseMap();
            CreateMap<Franchise, FranchiseGetAllDTO>()
                .ForMember(fdto => fdto.MovieId, opt => opt
                .MapFrom(f=>f.Movies.Select(m => m.MovieId)
                .ToArray()));
            
            CreateMap<Franchise, FranchiseGetDTO>()//.ReverseMap();
                .ForMember(fdto => fdto.MovieId, opt => opt
                .MapFrom(f => f.Movies))
                .ReverseMap();
            CreateMap<Franchise, FranchiseReadDTO>()//.ReverseMap();
                .ForMember(fdto => fdto.MovieId, opt => opt
                .MapFrom(f => f.Movies))
                .ReverseMap();

            CreateMap<Franchise, FranchisePostDTO>().ReverseMap();
            CreateMap<Franchise, FranchisePutDTO>().ReverseMap();
            //.ForMember(fdto => fdto.MovieId, opt => opt
            //.MapFrom(f => f.MovieId))
            //.ReverseMap();
            /* CreateMap<Movie, MovieGetAllDTO>()
                 .ForMember(m => m.CharacterId, opt => opt
                 .MapFrom(c => c.Characters.Select(c => c.CharacterId)
                 .ToArray()));*/
        }
    }
}
