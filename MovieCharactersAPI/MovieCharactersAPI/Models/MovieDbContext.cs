﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models.Domain;
using System.Collections.Generic;

namespace MovieCharactersAPI.Models
{
    public class MovieDbContext : DbContext
    {
        //setting up the Tables you want in your database
        public DbSet<Character> Characters { get; set; }
        public  DbSet<Movie> Movies{ get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        public MovieDbContext(DbContextOptions options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>().HasData(new Movie() { MovieId = 1, Director= "Matt Reeves", Genre="Crime", Picture= "https://m.media-amazon.com/images/M/MV5BYTExZTdhY2ItNGQ1YS00NjJlLWIxMjYtZTI1MzNlMzY0OTk4XkEyXkFqcGdeQXVyMTEyMjM2NDc2._V1_FMjpg_UX1000_.jpg", ReleaseYear="2022",Title= "The Batman", Trailer= "https://www.imdb.com/video/vi4130521881?playlistId=tt1877830&ref_=tt_pr_ov_vi", FranchiseId=1 }) ;
            modelBuilder.Entity<Movie>().HasData(new Movie() { MovieId = 2, Director = "Zack Snyder", Genre = "Sci-Fi", Picture = "https://m.media-amazon.com/images/M/MV5BYThjYzcyYzItNTVjNy00NDk0LTgwMWQtYjMwNmNlNWJhMzMyXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_.jpg", ReleaseYear = "2016", Title = "Batman v Superman: Dawn of Justice", Trailer = "https://www.imdb.com/video/vi1946858521?playlistId=tt2975590&ref_=tt_pr_ov_vi" , FranchiseId=2});
            modelBuilder.Entity<Movie>().HasData(new Movie() { MovieId = 3, Director = "Patty Jenkins", Genre = "Sci-Fi", Picture = "https://m.media-amazon.com/images/M/MV5BMjAyMjUyNzg1Ml5BMl5BanBnXkFtZTgwNzAwMzg5MTE@._V1_.jpg", ReleaseYear = "2017", Title = "Wonder Woman", Trailer = "https://www.imdb.com/video/vi3944268057?playlistId=tt0451279&ref_=tt_pr_ov_vi" , FranchiseId=3});

            modelBuilder.Entity<Character>().HasData(new Character() { CharacterId = 1,FullName="Bruce Wayne",Alias="Batman",Gender="Male",Picture= "https://i.guim.co.uk/img/media/ec27f69b7e4ac14838e8f71842a4cc6db3b8d69c/112_4_1179_708/master/1179.jpg?width=465&quality=45&auto=format&fit=max&dpr=2&s=80fc9fadf9fa45c93887ddd379009071" });
            modelBuilder.Entity<Character>().HasData(new Character() { CharacterId = 2, FullName = "Clark Kent", Alias = "Superman", Gender = "Male", Picture = "https://imageio.forbes.com/blogs-images/jvchamary/files/2016/03/man_of_steel-1200x800.jpg?fit=bounds&format=jpg&width=960" });
            modelBuilder.Entity<Character>().HasData(new Character() { CharacterId = 3, FullName = "Princess Diana of Themyscira", Alias = "Wonder Woman", Gender = "Female", Picture = "https://static.posters.cz/image/1300/plakater/wonder-woman-1984-solo-i97792.jpg" });

            modelBuilder.Entity<Franchise>().HasData(new Franchise() { FranchiseId = 1, Name = "Warner Bros/DC Comics", Movies = { }, Description = "DC Comics movies" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise() { FranchiseId = 2, Name = "Disney/Marvel", Movies = { }, Description = "Marvel Comics movies" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise() { FranchiseId = 3, Name = "Pixar", Movies = { }, Description = "Animation cartoon movies" });


            modelBuilder.Entity<Character>()
                .HasMany(c => c.Movies)
                .WithMany(m => m.Characters)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    rs => rs.HasOne<Movie>().WithMany().HasForeignKey("CharacterId"),
                    ls => ls.HasOne<Character>().WithMany().HasForeignKey("MovieId"),
                    je =>
                    {
                        je.HasKey("CharacterId", "MovieId");
                        je.HasData(
                            new { CharacterId = 1, MovieId = 1 },
                            new { CharacterId = 2, MovieId = 2 },
                            new { CharacterId = 3, MovieId = 3 }
                        );
                    }
                    );
                /*j => j.HaseOne<Character>.WithMany().HasForeignKey("MovieId"),
                l => l.HasOne<Movie>().WithMany().HasForeignKey("CharacterId"),*/
                /*je =>
                {
                    je.HasKey("CharacterId", "MovieId");
                    je.HasData(
                        new { CharacterId = 1, MovieId = 1 },
                        new { CharacterId = 1, MovieId = 2 }
                    );
                }*/
            //);

            /*.UsingEntity(e => e.HasData(
                new { MoviesMovieId = 1, CharactersCharacterId = 1 },
                new{  MoviesMovieId = 2, CharactersCharacterId = 2},
                new { MoviesMovieId = 3, CharactersCharacterId = 3 }
                ));*/

            /*modelBuilder.Entity<Movie>()
                .HasOne(m => m.Franchise)
                .WithMany(f => f.Movies)//.WithMany(m => m.FranchiseId)
                .HasForeignKey(m => m.MovieId);
                /*UsingEntity(e => e.HasData(
                    new { MoviesMovieId = 1, FranchiseFranchiseId = 1 },
                    new { MoviesMovieId = 2, FranchiseFranchiseId = 2 },
                    new { MoviesMovieId = 3, FranchiseFranchiseId = 3 }
                    ));*/
        }
    }
}
