﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models.Domain
{
    public class Franchise
    {
        public int FranchiseId { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        public string Description { get; set; }

       // public int? MovieId { get; set; }
        public ICollection<Movie> Movies { get; set; }
    }
}
