﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models.Domain
{
    public class Character
    {
        public int CharacterId { get; set; }
        [MaxLength(50)]
        public string FullName { get; set; }
        [MaxLength(50)]
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }

        public ICollection<Movie> Movies { get; set; }

    }
}
