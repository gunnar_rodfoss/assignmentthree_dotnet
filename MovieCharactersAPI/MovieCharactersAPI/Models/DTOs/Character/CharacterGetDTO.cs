﻿using System.Collections.Generic;

namespace MovieCharactersAPI.Models.DTOs
{
    public class CharacterGetDTO
    {
        public int? CharacterId { get; set; } = null;
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }

        public List<int> MovieId { get; set; }


    }
}
