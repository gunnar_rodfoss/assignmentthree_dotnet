﻿namespace MovieCharactersAPI.Models.DTOs
{
    public class CharacterPutDTO
    {
        public int CharacterId { get; set; }
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }
    }
}
