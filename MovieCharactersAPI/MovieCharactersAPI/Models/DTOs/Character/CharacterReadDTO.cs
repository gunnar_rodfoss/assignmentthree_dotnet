﻿namespace MovieCharactersAPI.Models.DTOs
{
    public class CharacterReadDTO
    {
        public int? CharacterId { get; set; } = null;
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }
    }
}
