﻿using System.Collections.Generic;

namespace MovieCharactersAPI.Models.DTOs
{
    public class MoviePostDTO
    {
        public string Title { get; set; }
        public string Genre { get; set; }
        public string ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        public int? FrenchiseId { get; set; }

        //public List<int> CharacterId { get; set; }
    }
}
