﻿

using System.Collections.Generic;

namespace MovieCharactersAPI.Models.DTOs
{
    public class FranchiseGetAllDTO 
    {
        public int FranchiseId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public List<int> MovieId { get; set; }

    }
}
