﻿namespace MovieCharactersAPI.Models.DTOs
{
    public class FranchisePostDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }

        //public int? MovieId { get; set; }
    }
}
