﻿namespace MovieCharactersAPI.Models.DTOs
{
    public class FranchiseReadDTO
    {
        public int FranchiseId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? MovieId { get; set; }
    }
}
