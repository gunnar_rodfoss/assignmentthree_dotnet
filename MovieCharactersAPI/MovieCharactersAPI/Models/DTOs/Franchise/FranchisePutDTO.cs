﻿namespace MovieCharactersAPI.Models.DTOs
{
    public class FranchisePutDTO
    {
        public int FranchiseId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        
    }
}
