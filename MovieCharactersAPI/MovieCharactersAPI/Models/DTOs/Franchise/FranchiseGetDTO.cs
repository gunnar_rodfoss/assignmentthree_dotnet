﻿using System.Collections.Generic;

namespace MovieCharactersAPI.Models.DTOs
{
    public class FranchiseGetDTO
    {
        public int FranchiseId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        //public int? MovieId { get; set; }
        public List<int> MovieId { get; set; }
    }
}
