﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTOs;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class MoviesController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper; 

        public MoviesController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all the Movies in the database
        /// </summary>
        /// <returns>Returns all the movies in a Database</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieGetAllDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieGetAllDTO>>(await _context.Movies.Include( m => m.Characters).ToListAsync() );
        }

        /// <summary>
        /// Gets all the Characters inn a Movie by giving a movie id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns all the Characters inn a Movie</returns>
        [Route("{id}/Characters")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterGetDTO>>> GetCharacters(int id)
        {
            var movie = await _context.Movies.Include(m => m.Characters).Where(m => m.MovieId == id).FirstAsync();

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<CharacterGetDTO>>(movie.Characters.ToList());

        }



        /// <summary>
        /// Get a movie by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns a movie by id</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieGetDTO>> GetMovie(int id)
        {
            var movie = await _context.Movies.Include(m => m.Characters).FirstOrDefaultAsync(m => m.MovieId == id);//.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieGetDTO>(movie);
        }
        /// <summary>
        /// Updates a movie in a Franchise
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        [HttpPut("{id}/add-franchise")]
        public async Task<ActionResult<MovieGetDTO>> AddFranchiseToMovie(int id, int franchiseId)
        {
            Movie movie = await _context.Movies.FindAsync(id);
            Franchise franchise = await _context.Franchises.FindAsync(franchiseId);

            if (movie == null || franchise == null)
            {
                return NotFound();
            }

            movie.FranchiseId = franchiseId;

            await _context.SaveChangesAsync();

            MovieGetDTO rdto = _mapper.Map<MovieGetDTO>(movie);
            return CreatedAtAction("GetMovie", new { id = rdto.MovieId }, rdto);
        }




        /// <summary>
        /// Updates a movie with a Character
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterId"></param>
        /// <returns></returns>

        [HttpPut("{id}/UpdateCharacter")]
        public async Task<ActionResult<MovieGetDTO>> PutCharactersInMovie(int id, int characterId)
        {
            Character character = await _context.Characters.FindAsync(characterId);
            Movie movie = await _context.Movies.Include(m => m.Characters).Where(m => m.MovieId == id).FirstAsync();

            if (movie == null || character == null)
            {
                return NotFound();
            }

            movie.Characters.Add(character);

            await _context.SaveChangesAsync();

            MovieGetDTO rdto = _mapper.Map<MovieGetDTO>(movie);
            return CreatedAtAction("GetMovie", new { id = rdto.MovieId }, rdto);
        }




        // POST: api/Movies
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754'
        /// <summary>
        /// Posts a new Movie in the database
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<MovieGetDTO>> PostMovie(MoviePostDTO movie)
        {
            Movie domainMovie = _mapper.Map<Movie>(movie);
            _context.Movies.Add(domainMovie);
            await _context.SaveChangesAsync();

            MovieGetDTO rdto = _mapper.Map<MovieGetDTO>(domainMovie);
            return CreatedAtAction("GetMovie", new { id = rdto.MovieId }, rdto);
        }

        // DELETE: api/Movies/5
        /// <summary>
        /// Delets a movie in the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }


        /// <summary>
        /// Delets a movie by setting FrenchiseId to null
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}/deleteFromFrenchise")]
        public async Task<ActionResult<MovieGetDTO>> DeleteMovieFromFrenchise(int id)
        {
            Movie movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            movie.FranchiseId = null;
 
            await _context.SaveChangesAsync();


            MovieGetDTO gdto = _mapper.Map<MovieGetDTO>(movie);
            return CreatedAtAction("GetMovie", new { id = gdto.MovieId }, gdto);

        }





        /// <summary>
        /// Delete Character from Character list in Movie
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterId"></param>
        /// <returns></returns>


        [HttpDelete("{id}/deleteCharacter")]
        public async Task<IActionResult> DeleteCharacterFromMovie(int id, int characterId)
        {
            Character character = await _context.Characters.FindAsync(characterId);
            Movie movie = await _context.Movies.Include(m => m.Characters).Where(m => m.MovieId == id).FirstAsync();

            if (movie == null || character == null)
            {
                return NotFound();
            }

            movie.Characters.Remove(character);

            await _context.SaveChangesAsync();

            return NoContent();
        }





        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.MovieId == id);
        }
    }
}
