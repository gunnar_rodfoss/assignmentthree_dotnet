﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTOs;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class CharacterController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public CharacterController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Returns all the Characters in the Characters table
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterGetDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterGetDTO>>( await _context.Characters.Include(c => c.Movies).ToListAsync() );
        }

        /// <summary>
        /// Returns a Character from the character tabel
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterGetDTO>> GetCharacter(int id)
        {
            
            var character = await _context.Characters.Include(c => c.Movies).FirstOrDefaultAsync(c => c.CharacterId == id);// .FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterGetDTO>(character);
        }

        /// <summary>
        /// Inserts values into a character in the character table
        /// </summary>
        /// <param name="id"></param>
        /// <param name="character"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterPutDTO character)
        {
            if (id != character.CharacterId)
            {
                return BadRequest();
            }
            var domainCharacter = _mapper.Map<Character>(character);
            _context.Entry(domainCharacter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Inserts a new character in the character table
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CharacterGetDTO>> PostCharacter(CharacterPostDTO character)
        {
            Character domainCharacter = _mapper.Map<Character>(character);
            _context.Characters.Add(domainCharacter);
            await _context.SaveChangesAsync();

            CharacterGetDTO rdto = _mapper.Map<CharacterGetDTO>(domainCharacter);
            return CreatedAtAction("GetCharacter", new { id = rdto.CharacterId }, rdto);
            
        }

        /// <summary>
        /// Delets a character in the 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.CharacterId == id);
        }
    }
}
